.PHONY: help

red=`tput setaf 1`
green=`tput setaf 2`
magenta=`tput setaf 5`
blue=`tput setaf 6`
yellow=`tput setaf 3`
reset=`tput sgr0`

API_PATH=api/
CLIENT_PATH=client/
WEBHOOK_PATH=webhook/
NODE_MODULES=node_modules/

run: run-api run-client run-webhook

run-api:
	@echo "${green}------ RUN TERAVOZ-API ---${reset}"
	cd $(API_PATH) && yarn start

run-client:
	@echo "${green}------ RUN TERAVOZ-CLIENT ---${reset}"
	cd $(CLIENT_PATH) && yarn start

run-webhook:
	@echo "${green}------ RUN WEBHOOK ---${reset}"
	cd $(WEBHOOK_PATH) && yarn start

clean:
	@echo "${green}------ CLEAN PACKAGES ${reset}"
	rm -Rf $(API_PATH)$(NODE_MODULES)
	rm -Rf $(CLIENT_PATH)$(NODE_MODULES)
	rm -Rf $(WEBHOOK_PATH)$(NODE_MODULES)
	@echo "${green}------ FINISH${reset}"

install:
	@echo "${green}------ INSTALL DEPENDENCIES${reset}"
	cd $(API_PATH) && yarn
	cd $(CLIENT_PATH) && yarn
	cd $(WEBHOOK_PATH) && yarn
	@echo "${green}------ FINISH INSTALL DEPENDENCIES${reset}"

help: 
	@echo "${magenta}make clean${reset} faz a limpeza da node_modules nos 3 diretórios (/api /client /webhook)"
	@echo "${magenta}make install${reset} roda o npm install nos projetos (/api /client /webhook)"
	@echo "${magenta}make run-api${reset}  Sobe a teravoz-api na porta 3334"
	@echo "${magenta}make run-api ${yellow}PORT=%YOUR_PORT%${reset} Sobe a teravoz-api na porta informada no local do %YOUR_PORT%"
	@echo "${magenta}make run-webhook${reset} Sobe o webhook na porta 3334"
	@echo "${magenta}make run-webhook ${yellow}PORT=%YOUR_PORT%${reset} Sobe o webhook na porta informada no local do %YOUR_PORT%"
	@echo "${magenta}make run-client${reset}  O Client passa a ser iniciado."

call:
	@echo "Fazendo a ligação para: ${blue}$(phone)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(phone)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions


PHONE_1=11999990001
PHONE_2=11999990002
PHONE_3=11999990003
PHONE_4=11999990004
PHONE_5=11999990005
PHONE_6=11999990006
test:
	@echo "Fazendo a ligação para: ${blue}$(PHONE_1)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(PHONE_1)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions
	@echo "Fazendo a ligação para: ${blue}$(PHONE_2)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(PHONE_2)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions
	@echo "Fazendo a ligação para: ${blue}$(PHONE_3)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(PHONE_3)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions
	@echo "Fazendo a ligação para: ${blue}$(PHONE_4)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(PHONE_4)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions
	@echo "Fazendo a ligação para: ${blue}$(PHONE_5)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(PHONE_5)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions
	@echo "Fazendo a ligação para: ${blue}$(PHONE_6)${reset}"
	@curl -X POST -H 'Content-type: application/json' -d '{ "type": "dialer", "numbers": [ "$(PHONE_6)" ], "destination": "100", "destination_type": "peer", "code": "teste-01", "retries": "1", "retry_gap": "10s", "ttl": "5m" }' http://localhost:3334/actions