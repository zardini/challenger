# Hello Teravoz

First, thank you for the opportunity to take the test, I am very happy for the opportunity.

**Teravoz API** responsible for creating events.
**Teravoz Client** Client of the application, in it we can see the extensions and if they are busy (in connection, or available).
**Webhook** Here is the heart. Webhook awaits the events issued by the API and on top of that it takes the decision of the delegate and saves the phone numbers to assist in that decision.

The Makefile has some commands to assist you in running the project.
`make clean` Make clean in node_modules in the directories (/teravoz-api /teravoz-client /webhook)

`make install` Run the npm install in the projects (/teravoz-api /teravoz-client /webhook)

`make run-api` Run teravoz-api on listen port 3333

`make run-api PORT=%YOUR_PORT%` Run the teravoz-api on port argument

`make run-webhook` Run the webhook on listen port 3334

`make run-webhook PORT=%YOUR_PORT%` Run the webhook on port argument

`make run-client` Run client

`make call phone=NUMBER` Run curl call usign phone in argument

`make test` Run 6 calls for testing.


It is also possible to execute the 3 at the same time using the command `make -j 3 run` so it runs 3 processes and monitor at the same time.

`make help` Show principal commands