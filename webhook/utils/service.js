const axios = require('axios');
const Config = require('./config.json');

module.exports.send = (id, delegate, res) => {
    const baseDelegate = {type: "delegate", call_id: id };
    
    if(!delegate) {
        console.error(err);
        res.status(400).json({ error: "Bad request" });
    }

    // Send destination to teravoz-api/actions.
    axios.post(Config.TeravozApi, {...baseDelegate, destination: delegate}).then(() => {
        res.status(200).json({ delegate });
    }).catch((err) => {
        console.error(err);
        res.status(500).json({ error: "Internal error" });
    });

    return true;
}