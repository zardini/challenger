/**
 * This code based in: teravoz client example
 * github: https://github.com/teravoz/teravoz-client-example
 * @author: Teravoz
 */

const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const service = require('./utils/service.js');
const bodyParser = require('body-parser');
let Datastore = require('nedb')
  , db = new Datastore({ filename: 'persistence/db.db' });

const PORT = process.env.PORT || 3333;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Loading database 
 */
db.loadDatabase();

/**
 * This func check if why ramal delegate a message.
 * 
 * @param  {json} event
 */
function delegate(event, res) {
  let obj = { number: event.their_number };
  db.find(obj, (e, data) => {
    let delegate_number;
    if(data.length > 0) { // Number found - 901
      delegate_number = 901;
    } else {              // Not found - 900
      db.insert({ number: event.their_number });
      delegate_number = 900;
    }
    service.send(event.call_id, delegate_number, res);
    return;
  });
}

/**
  POST http://localhost:port/webhook
  Webhook para receber eventos.
*/
app.post('/webhook', (req, res) => {
  // Evento está no body do request
  const event = req.body;
  if (!event) {
    res.status(400).json({error: "Bad request"});
  }
  // Emit event for client in reactjs.
  io.emit(`teravoz-event`, event);
  if(event.type == "call.standby") {
    delegate(event, res);
  } else {
    res.sendStatus(204);
  }
});

http.listen(PORT, function(){
  console.info('Listen *:' + PORT);
});