# Webhook - Teravoz

Inside this application, we found a simple webhook. This receive the events of actions and make the decision of **delegate** call with base in number called, if this number exist in database, webhook transfers call to extension `901`, else transfers call to extension `900`

It's structure is as follows:
```
├── index.js (Webhook)
├── persistence
│   └── db.db (nedb)
├── test
│   ├── index.js
│   └── config.json
└── utils
    ├── service.js
    └── config.json (Configuring service.js)

```


The main ones using libraries and yours documentation

- [Express](https://expressjs.com/pt-br/4x/api.html)
- [Socket.io](https://socket.io/docs/)

    

## Routes and Status code.
```
/webhook
├── 204
│   └── Success. (No content)
├── 200
│   └── { delegate: ramal }
├── 400
│   └── { error: Bad request }
└── 500
    └── { error: internal error }
```
  
## How run this project ?

**It's simple**
for install components: `yarn` or `npm install`

For running `node run start` or `yarn start`

In testing, we using Mocha, if you have installed ignore, else, run:`npm install -g mocha`

For testing `mocha test/` or `yarn test` or `node run test`

Just all of it.