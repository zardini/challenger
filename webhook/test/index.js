var Config = require('./config.json');
const axios = require('axios');


// This is
describe('Webhook aplication', () => {

    it('Is live ?', (done) => {  
        axios.post( Config.Webhook, Config.TestEvent ).then(() => done())
        .catch(() => done(new Error("Webhook - Server is Down.")));
    });

    it('Delegate 900 ?', (done) => {
        axios.post( Config.Webhook, Config.Delegate900 ).then((response) => {
            if(response.status == 200 && response.data.delegate === 900) {
                done()
            } else if(response.status == 200 && response.data.delegate === 901) {
                done(new Error("Teravoz API - Delegate to 901."));
            } else {
                done(new Error("Teravoz API - No delegate.", response.status));
            }
        }).catch(() => done(new Error("Teravoz API - Server is Down.")));
    });

    it('Delegate 901 ?', (done) => {  
        axios.post( Config.Webhook, { ...Config.Delegate901, their_number: Math.floor(Math.pow(10, 10) + Math.random() * (Math.pow(10, 11) - Math.pow(10, 10) - 1))}).then((response) => {
            if(response.status == 200 && response.data.delegate === 901) {
                done()
            } else if(response.status == 200 && response.data.delegate === 900) {
                done(new Error("Teravoz API - Delegate to 900."));
            } else {
                done(new Error("Teravoz API - No delegate.", response.status));
            }
        }).catch(() => done(new Error("Teravoz API - Server is Down.")));
    });

  
  });