const app = require('express')();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const emitEvents = require('./utils/emit-events');

const PORT = process.env.PORT || 3334;
const WEBHOOK = process.env.WEBHOOK || 'http://localhost:3333/webhook';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Unique route
app.post('/actions', (req, res) => {
  
  const event = req.body; // get Event

  if(event.type == 'dialer') {
    emitEvents(WEBHOOK, event, "dialer");
  } else if(event.type == 'delegate') {
    emitEvents(WEBHOOK, event, "delegate");
  } else {
    console.log("No dialer or Delegate -> ignore event")
  }

  res.json({ status: 'ok' });
});

http.listen(PORT, function(){
  console.info('Listening on *:', PORT);
});