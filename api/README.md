# API - Teravoz

Projeto da API foi utilizado como base o do teste do Front-end.

A estrutura foi feita da seguinte maneira
```
├── utils  - Funções de apoio
│   └── ...
├── mockup.json - JSON com os eventos básicos.
└── index.js    - API que recebe os parametros posts.
```

## How run this project ?

**It's simple**
for install components: `yarn` or `npm install`

For running `node run start` or `yarn start`
  
Just all of it.