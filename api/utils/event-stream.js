const BASE_EVENTS = require('../mockup.json');

var persistence = {}

module.exports.create = (event, type) => {
  let streamFn = function(event, type) {
    let currentIndex = 0;
    let callId = event.call_id || `${new Date().getTime()}.${event.numbers[0]}`;
    if(event.type == 'dialer') {
      // NEW CALL
      persistence[callId] = { phone: event.numbers[0], time: new Date().toISOString() };
    } else if(!persistence[callId] && event.their_number) {
      persistence[callId] = { phone: event.their_number, time: new Date().toISOString() };
    }

    // returns next event
    this.next = () => {
      let currentEvent = BASE_EVENTS[event.type][currentIndex];
      currentEvent.call_id = callId; // Set new id or exist id.
      currentIndex++;

      currentEvent.timestamp = persistence[callId] ? persistence[callId].time : new Date().toISOString();
      if (currentEvent.type.startsWith('actor')) {
        currentEvent.number = event.destination;
        currentEvent.actor = `${event.destination}@teracompra.com`;
      } else {
        currentEvent.their_number = persistence[callId] ? persistence[callId].phone : "";
      }

      if(currentEvent.type == 'call.finished') {
        delete persistence[event.call_id];
      }
      console.log(currentEvent);
      return currentEvent;
    }

    this.hasNext = () => {
      return currentIndex < BASE_EVENTS[type].length;
    }

    // time gap for next event
    this.gap = () => {
      // milliseconds between 1000 and 10000
      return (Math.floor(Math.random() * 10) + 1) * 1000;
    }
  }

  return new streamFn(event, type);
}