const axios = require('axios');
const EventStream = require('./event-stream');

/**
 * Sends call events to the URL 'webhook'
 */
module.exports = function emitEvents(webhook, event, type) {
  let events = EventStream.create(event, type);
  sendEvent(events, webhook);
}

function sendEvent(events, webhook) {
  let nextEvent = events.next();

  console.info(`POSTing event `, nextEvent);
  axios.post( webhook, nextEvent).then((response) => {
    console.log(`POST status code response.`);
  }).catch((err) => {
    // HAS ERROR ?
    console.error('ERROR_CODE:', err.code);
    console.error('ERROR_MSG:', err.message);
    console.error('ERROR:', err);
  });

  if (events.hasNext()) {
    // Another event within 'gap' milliseconds
    setTimeout(() => {
      sendEvent(events, webhook);
    }, events.gap());
  }
}