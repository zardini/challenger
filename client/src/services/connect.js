import openSocket from 'socket.io-client';
import Config from "../config.json";
const socket = openSocket(Config.websocket); // This is URL Websocket.

/**
 * This function waits for the events of the websocket, 
 * if this event is 'teravoz-event' sent to the callback
 * 
 * @param   {function} cb - Callback function
 *
 * @return  {message{}} - Send messae if event is teravoz-event
 */
function connect(cb) {
  socket.on('teravoz-event', (message) => {
    cb(message);
  })
}

export { connect }