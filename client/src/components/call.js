import React, { PureComponent } from 'react';

class Call extends PureComponent {
  render() {
    const { id, call } = this.props;

    if(!call && !id) {
        console.info("Bad props");
        return null;
    }

    return (
        <div>
            <div className="call">
            <div className="col"><p><b>ID:</b> {id}</p></div>
            <div className="col"><p><b>Type:</b> {call.type}</p></div>
            <div className="col"><p><b>Our number:</b> {call.our_number}</p></div>
            <div className="col"><p><b>Their number:</b> {call.their_number}</p></div>
            <div className="col"><p><b>Direction:</b> {call.direction}</p></div>
            <div className="col"><p><b>Date:</b> {call.date}</p></div>
            </div>
        </div>
    );
  }
}

export default Call;
