import React, { PureComponent } from 'react';

class Actor extends PureComponent {
  render() {
    const { actor, callId, openCall } = this.props;
    
    if(!actor) {
        console.info("Bad props");
        return null;
    }

    return (
        <div className="box">
            <h2>{actor.name}</h2>
            <p><b>Ramal:</b> {actor.number}</p>
            {callId ?
                <p className="status status-red" onClick={() => openCall(callId)}>Em ligação</p>
            : 
                <p className="status status-green">Disponivel</p>
            }
        </div>
    );
  }
}

export default Actor;
