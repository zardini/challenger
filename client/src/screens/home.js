import React, { PureComponent } from 'react';
import { connect } from '../services/connect';
import Call from '../components/call';
import Actor from '../components/actor';

/**
 * This Main Component
 * @class App
 * @extends PureComponent
 */
class App extends PureComponent {

  constructor(props) {
    super(props);

    this.screen = this.screen.bind(this);
    this.listener = this.listener.bind(this);

    this.renderCall = this.renderCall.bind(this);
    this.renderCallers = this.renderCallers.bind(this);

    this.state = {
      activeScreen: "active-calls",
      openCall: "",
      calls: [],
      peersOnCalls: [],
      peers: [
        {
          "name": "João",
          "number": "900"
        },
        {
          "name": "José",
          "number": "901"
        }
      ]
    }

  }

  /**
   * HANDLERS FUNCTIONS
   * @param  {} screen
   */
  screen = (screen) => {
    this.setState({ activeScreen: screen });
    this.forceUpdate();
  }

  /**
   * @param  {} id
   */
  openCall = (id) => {
    this.setState({ openCall: id });
  };

  componentDidMount() {
    connect(message => this.listener(message));
  }

  /**
   * WEBSOCKET FUNCTIONS
   */
  // WEBSOCKET LISTENER EVENTS 
  listener = (message) => {
    if (message.type.indexOf("actor") > -1) {
      this.receivedActor(message);
    } else {
      this.receivedCall(message);
    }
  }

  // WEBSOCKET HELPER - RECEVED ACTOR ACTION
  receivedActor = (message) => {
    let { peersOnCalls } = this.state;
    peersOnCalls[message.number] = message.call_id;

    if (message.type === "actor.left") {
      if (this.state.openCall === message.call_id) {
        this.setState({ openCall: "" });

      }
      delete peersOnCalls[message.number];
    }

    this.setState({ peersOnCalls: peersOnCalls });
    this.forceUpdate();

  }

  // WEBOSKCET HELPER - RECEIVED CALL ACTION
  receivedCall = (message) => {
    let timeEvent = new Date(message.timestamp);

    let call = {
      type: message.type,
      direction: message.direction,
      our_number: message.our_number,
      their_number: message.their_number,
      date: timeEvent.toISOString()
    }


    let calls = this.state.calls;
    calls[message.call_id] = call;

    if (message.type === "call.finished") {
      delete calls[message.call_id];
    }

    this.setState({ calls: calls });
    this.forceUpdate();
  }

  /**
   * RENDER FUNCTIONS
   */
  renderCallers = () => {
    // Get all peers
    let peers = this.state.peers.map((p) => {
      return (
        // Render peers component
        <Actor
          key={p.number}
          actor={p}
          callId={this.state.peersOnCalls[p.number] || ''}
          openCall={this.openCall}
        />
      );
    });

    return (
      // Holder callers
      <div id="callers">
        {peers}
      </div>
    );
  }

  renderActiveCall = () => {
    let keys = Object.keys(this.state.calls);

    if(keys.length <= 0) {
      return (
        <div id="calls">
          <p style={{textAlign: "center"}}>Não tem nenhuma ligação em andamento no momento.</p>
        </div>)
    }

    let calls = keys.map((key) => {
      return (
        // Render peers component
        <Call
          key={key}
          id={key}
          call={this.state.calls[key]}
        />
      );
    });

    return (
      // Holder callers
      <div id="calls">
        {calls}
      </div>
    );
  }

  renderCall = (id) => {
    const call = this.state.calls[id];

    return (
      // render calls
      <Call
        key={id}
        id={id}
        call={call}
      />
    );
  }

  render() {

    // Main render
    const { activeScreen, openCall } = this.state
    let screen;
    if (activeScreen === "callers") {
      screen = this.renderCallers();
    } else {
      screen = this.renderActiveCall();
    }

    return (
      <div className="main">
        <header>
          <div className="brand"><img src="https://www.teravoz.com.br/assets/images/logotipo_teravoz.png" width="auto" height="80" alt="Brand Teravoz" /></div>
          <div className="menu">
            <a href="#callers" className={activeScreen === "callers" ? "active" : ""} onClick={() => this.screen("callers")}>Callers</a>
            <a href="#active-calls" className={activeScreen === "active-calls" ? "active" : ""} onClick={() => this.screen("active-calls")}>Active calls</a>
          </div>
        </header>
        <div className="container">
          {openCall && this.renderCall(openCall)}
          {screen}
        </div>
      </div>
    );
  }
}

export default App;
