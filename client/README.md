# Client - Teravoz

O Client foi desenvolvido usando React.JS

A estrutura foi feita da seguinte maneira
```
├── components  - (Aqui irá ficar todos os web components da aplicação)
│   └── ...
├── css         - (Os arquivos .css ficam centrados nesse diretório)
│   └── ...
├── screens     - (As screens (cenas princiapis) ficam localizados aqui)
│   └── ...
├── services    - (Ligações com serviçøs externos ficam centralizados ai para facilitar a manutenção)
│   └── ...
├── config.json - Configurações da aplicação.
└── index.js    - Root, starta a aplicação propriamente dita.
```
 
## How run this project ?

**It's simple**
for install components: `yarn` or `npm install`

For running `node run start` or `yarn start`
  
Just all of it.